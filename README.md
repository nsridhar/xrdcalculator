# USAGE #
Need SHELLX format (.res) files to use XRDCalculator.

##1) Single resfile##
python2.7 diffraction.py -xrd_res filename.res

##2) File-list##
python2.7 diffraction.py -xrd_res_sl filelist 
(First column should contain all resfiles)


### Description ###

* Written to match predicted pharmaceutical crystal structures to measured pXRD patterns. 

# BUGS #
1) Major bugfix with detecting equivalent reflections and calculating corresponding multiplicities.
2) Distance matrix not needed for calculating PBCs. Speed-up ~ 10X.

#TO-DO#
1) Relative intensities are not well produced inspite of adding 4 different correction factors.`