from __future__ import division, unicode_literals
from molecule import Molecule
from crystal import  CrystalStructure,Lattice
from math import sin, cos
import math
import numpy as np
import argparse
import os


#XRD wavelengths in angstroms
 #   "CuKa": 1.54184,
#   "CuKa2": 1.54439,
#  "CuKa1": 1.54056,
#  "CuKb1": 1.39222,
 # "MoKa": 0.71073,
 #"MoKa2": 0.71359,
 #"MoKa1": 0.70930,
  #"MoKb1": 0.63229,
  #"CrKa": 2.29100,
  #"CrKa2": 2.29361,
  #"CrKa1": 2.28970,
 #"CrKb1": 2.08487,
  #"FeKa": 1.93735,
  #"FeKa2": 1.93998,
  #"FeKa1": 1.93604,
  #"FeKb1": 1.75661,
  #"CoKa": 1.79026,
  #"CoKa2": 1.79285,
  #"CoKa1": 1.78896,
  #"CoKb1": 1.63079,
  #"AgKa": 0.560885,
  #"AgKa2": 0.563813,
  #"AgKa1": 0.559421,
  #"AgKb1": 0.497082,
#}#



class XRDCalculator(object):

#    Computes the XRD pattern of a crystal structure from a .res file..

# Usage:
#python diffraction.py -res_xrd filename





 #  This code is implemented by  Sridhar Neelarmaju as part of CSPy  crystal structure prediction software.
# Reference:
 #Structure of MaterialsAuthor: Marc De Graef, Michael E. McHenry. Pages: 774 Size: 93 MB Publisher: Cambridge University Press
   #Published: Aug 31, 2012

    #BASIC ALGORITHM FOR CALCULAING i(2THETA)):

    #1.Get hkl list from lfloppy box model
    #2. Calculate structure factor for a given hkl F(hkl)
    #3. Apply correctiions  to F(hkl) - 4 types are considered. Debywe-waller, lorentz polarisation, multiplicity and absorption.
    #4) Get intensities I(hkl) as modulus structure factor squared*corrections
    #5) Get I(2theta) from summing over all I(hkl)s given a delta-theta.





    def get_cell_vec(self,resfile):

        #1)generate a direct basis cell vector from resfile
        X=CrystalStructure()
        mol=Molecule()
        init=X.init_from_res_file(resfile)
        res_data=X.res_list_form(1.0,False)
        kptitle=res_data[0].split()
        cell= res_data[1].split()
        a2r = 0.0174532925

        a=float(cell[2])
        b=float(cell[3])
        c=float(cell[4])
        alpha=float(cell[5])
        beta=float(cell[6])
        gamma=float(cell[7])
        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma
        #scaled volume of cell
        v = math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                      + 2*cos(alpha)*cos(beta)*cos(gamma))
        #cell vectors in x y and z.
        #fix this!!!!!!(only idf orthogonal.)
        nacl_test=False
        if nacl_test:
            a=self.nacl_a()
            b=self.nacl_b()
            c=self.nacl_c()
            alpha=self.nacl_alp()
            beta=self.nacl_bet()
            gamma=self.nacl_gam()
            alpha = a2r * alpha
            beta  = a2r * beta
            gamma = a2r * gamma
        l1=[a,0,0]
        l2=[0,b,0]
        l3=[0,0,c]
        #move to fractionals, all Rj
        l=np.vstack(([l1],[l2],[l3]))
        return l

    def recip_cell(self, l):
    #2) rGenerate eciprocal basis vectors from direct basis vector l.
        l1=l[0]
        l2=l[1]
        l3=l[2]
        pi = math.pi
        cell = np.matrix([
                             [l1[0],l1[1],l1[2]],
                             [l2[0],l2[1],l2[2]],
                             [l3[0],l3[1],l3[2]]
                         ],dtype = float)
        cross = np.cross(l2,l3)
        vol = np.dot(l1,cross)
        rcell = np.empty_like(cell,dtype = float)
        a = cell[0,:]
        b = cell[1,:]
        c = cell[2,:]
          #checke 2*pi/vol factor
    #rcell is just a*,b*.c*
        rcell[0,:] = 2*pi/vol * np.cross(b,c)
        rcell[1,:] = 2*pi/vol * np.cross(c,a)
        rcell[2,:] = 2*pi/vol * np.cross(a,b)
        return rcell

    def get_fractional_coords(self,resfile):
        X=CrystalStructure()
        mol=Molecule()
        init=X.init_from_res_file(resfile)
        res_data=X.res_list_form(1.0,False)
        kptitle=res_data[0].split()
        cell= res_data[1].split()
        a2r = 0.0174532925

        a=float(cell[2])
        b=float(cell[3])
        c=float(cell[4])
        alpha=float(cell[5])
        beta=float(cell[6])
        gamma=float(cell[7])
        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma

        #import as cartesian from unique_molecule_list
        #print "Number of molecules:", len(X.unique_molecule_list)

        if len(X.unique_molecule_list) > 1:
            print 'Found more than one molecule in unique_molecule_list. EXIT. Not sure how to handle this atm.'
        #    exit()

        #Get fractional coordinates for whole cell.
        X.generate_unit_cell_molecules()
        #print 'Number of molecules in crystallographic cell =', len(X.molecule_list)
        for mol in X.molecule_list:
            U_cart=[atom.xyz for atom in mol.atoms]
            #print U_cart
           #cartesian to fractional
            U_frac=self.cart_to_frac(U_cart,a,b,c,alpha,beta,gamma)

        #center art a given cartesian coordinate?
            #testing with NaCl
            #1/2, 0, 0;	0, 1/2, 0;	0, 0, 1/2;	1/2, 1/2, 1/2.
        #U_frac=np.empty([8,3],dtype=float)



        return U_frac

    def cart_to_frac(self, U_cart,a,b,c,alpha,beta,gamma):
       #rdegtoradians
        a2r = 0.0174532925
        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma

        natom=len(U_cart)
        #print a,b,c,alpha,beta,gamma
        # (scaled) volume of the cell
        v = math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                      + 2*cos(alpha)*cos(beta)*cos(gamma))
        tmat = np.matrix([[ 1.0 / a,-cos(gamma)/(a*sin(gamma)), (cos(alpha)*cos(gamma)-cos(beta)) / (a*v*sin(gamma))  ],
                          [ 0.0,     1.0 / b*sin(gamma),         (cos(beta) *cos(gamma)-cos(alpha))/ (b*v*sin(gamma))  ],
                          [ 0.0,     0.0,                        sin(gamma) / (c*v)                                    ] ]
        )

        U_cart = np.array(U_cart)
        U_cart=np.reshape(U_cart,[natom,3])

        U_frac = U_cart * tmat.T
        U_frac = np.array(U_frac)
        #print U_frac
        return U_frac


    def get_com(self, U_molecule):
       #rdegtoradians
               #print   "in com:"
        ##assuming equal qeights,
       U_m=U_molecule
       Ucom_m = 0
       Ucom_m += U_m.sum(0) / len(U_m)

       return Ucom_m

    def count_cells(self, U, R0, l,U_frac):
        #U = cartesian coordinates of point about which a sphere is drawn.
        #R0= radius of sphere.
        #l = box vectors [3x3 matrix]
        #
        #   Find all points within a sphere of radius R0 from the point U (cartesian) taking into account   periodic boundary conditions. This includes sites in other perioic   images.
        # Thi s is used to calculate list of images of a given point with PBCs.

        #see DeGraaf, Floppy box monte-carlo paper.

        #consider 8 vertices of cube cn
        #
        #shift coordinates such that U is at origin.
        U_center=np.array(U)

        U_cart=np.dot(U_frac,l)
        U_cart_shifted=U_cart-U
        l1=[l[0,0],l[0,1],l[0,2]]
        l2=[l[1,0],l[1,1],l[1,2]]
        l3=[l[2,0],l[2,1],l[2,2]]

        #get cartesian unit vectors:
        x_cap=l1/np.linalg.norm(l1)
        y_cap=l2/np.linalg.norm(l2)
        z_cap=l3/np.linalg.norm(l3)
        #print "norm of x",x_cap,y_cap,z_cap
        #print l1,l2,l3
        #+-x,+-y,+-z (8 directions)

        cn = 2*R0 * np.matrix([ [1, 1, 1],  [-1, 1, 1],  [1, -1, 1], [1, 1, -1],  [-1, -1, 1],  [1, -1, -1],  [-1, 1, -1],  [-1, -1, -1] ], dtype=float)

        c0 = np.array(cn[0])
        c1 = np.array(cn[1])
        c2 = np.array(cn[2])
        c3 = np.array(cn[3])
        c4 = np.array(cn[4])
        c5 = np.array(cn[5])
        c6 = np.array(cn[6])
        c7 = np.array(cn[7])
       #this cube envelopes the sphere at origin with radius R0.
        #here the box vectors are in columns(NOTE)
        M = np.matrix([
            [l1[0], l2[0], l3[0]],
            [l1[1], l2[1], l3[1]],
            [l1[2], l2[2], l3[2]]
        ])
        #invert matrix
        try:
            Minv = np.linalg.inv(M)
        except np.linalg.LinAlgError:
            print "Can't invert cell vector matrix. STOP."
            exit()

        #8 vertices of parallelepiped.

        p0 = Minv*c0.T
        p1 = Minv*c1.T
        p2 = Minv*c2.T
        p3 = Minv*c3.T
        p4 = Minv*c4.T
        p5 = Minv*c5.T
        p6 = Minv*c6.T
        p7 = Minv*c7.T

        #x = np.array(U[:,0],dtype=float)
        natom=len(U_cart)
        #print "Minv", Minv.shape
        #print "cn",c0.shape

        #x = np.reshape(U_frac[:, 0], [natom, 1])
        #y = np.reshape(U_frac[:, 1], [natom, 1])
        #z = np.reshape(U_frac[:, 2], [natom, 1])


        #print "x", x[11],len(U)

        p = np.reshape(np.array([p0, p1, p2, p3, p4, p5, p6, p7], dtype=float), [8, 3])
        #print p



        #apply inverse to 8 vertices of the cube.
        N1 = []
        N2 = []
        N3 = []
        for i in range(8):
               #calculate max dotproduct N1 = max(pn,x), N2 = max(pn,y), N3=max(pn,z)
                N1.append(p[i, 0])
                N2.append(p[i, 1])
                N3.append(p[i, 2])

        N1 = np.ceil(np.amax(N1))
        N2 = np.ceil(np.amax(N2))
        N3 = np.ceil(np.amax(N3))
        #return number of cells in each direction.
        #print N1


        #print N1,N2,N3
        #N1 = 1
        #2 = 1
        #N3 = 1

        N_cell = np.array([N1, N2, N3], dtype=int)
        #N_cell = [2,2,2]
        print "Number of cells needed in each direction =",N_cell


        return N_cell

    def count_cells_cspy(self):
        lattice_vector_squared = numpy.dot(numpy.transpose(numpy.array(self.crystal.lattice.lattice_vectors)),numpy.array(self.crystal.lattice.lattice_vectors))
        print '--> Eigenvalues of the lattice matrix are '+str(numpy.linalg.eig(lattice_vector_squared)[0])
        mu_min = min(numpy.linalg.eig(lattice_vector_squared)[0])
        mu_min = mu_min ** 0.5


    def make_image(self, U, N, l, R0):
        #given the number of boxes in each direction, returns image list . [N = [Nx,Ny,Nz]]
        #See "Determining nearest image in non-orthogonal periodic systems", Mihaly Mezei.
        #Only one point U is used here.
        #To loop over all atoms iin cell, replace U with U_fac and loop over all atoms.

        N1=N[0]
        N2=N[1]
        N3=N[2]
        l1=[l[0,0],l[0,1],l[0,2]]
        l2=[l[1,0],l[1,1],l[1,2]]
        l3=[l[2,0],l[2,1],l[2,2]]
        H = np.matrix([
            [l1[0], l2[0], l3[0]],
            [l1[1], l2[1], l3[1]],
            [l1[2], l2[2], l3[2]]
        ])
        #print "Cell vectors: \n ", H.T
        Pim = []
        #sum over images in all directions.
        count = 0
        for i1 in range(-N1+1, N1 + 1):
            for i2 in range(-N2+1, N2 + 1):
                for i3 in range(-N3+1, N3 + 1):
                    count = count+1
                    #construct list of images
                    sum = np.array(U + np.dot(H, [i1, i2, i3]))
                    #sum is always an array of length natom_mol
                    Pim.append(np.array(sum))
                    dist=np.sqrt(np.sum((sum-U)**2))
                    if dist<R0:
                        print dist


        nbox = count
        Pim = np.array(Pim,dtype = float)
      #  print Pim

        #print "Pim length = ", len(Pim)
        #primary stores unit cell coordinates of whichever image you like.
        print "Number of boxes=", nbox
        #for all particles in cell, loop over all atoms and set U as U_frac
        natoms=len(U)
        Pim = np.reshape(Pim,[nbox*natoms,3])
        Hinv = np.linalg.inv(H)
        Pim_frac=np.dot(Pim,Hinv)
        Pim_frac=np.array(Pim_frac,dtype=int)
        oned_Pim = np.reshape(Pim,[nbox*natoms*3,1])

        #Pim contains list of all images in neigbhbourhood and primary cell.

        print "No. of atoms in image list", len(Pim)


        #self.get_energyenergy(Pim,R0,l1,l2,l3)

        return np.array(Pim_frac)


    def get_dhkl(self,lattice_type,h,k,l,a,b,c,alpha,beta,gamma):
        #print '>>in get_dhkl'

        nacl_Test=False
        if nacl_Test:
            a=self.nacl_a()
            b=self.nacl_b()
            c=self.nacl_c()
            alpha=self.nacl_alp()
            beta=self.nacl_bet()
            gamma=self.nacl_gam()
            lattice_type = 1
        ####################

        #returns lattice type:

        #lattice_type=self.get_lattice_type(resfile)

        #cubic
        if lattice_type==1:
            dhkl=a/np.sqrt((h**2+k**2+l**2))
        #hexagonal
        elif lattice_type ==7:
            dhkl=(a*c)/(np.sqrt(4*h*h*c*c+4*h*k*c*c+4*k*k*c*c+3*l*l*a*a))

        #tetragonal
        elif lattice_type==5:
            dhkl=(a*c)/np.sqrt((h*h*c*c + k*k*c*c + l*l*a*a))
        #orthorhombic
        elif lattice_type==4:
            dhkl=(a*b*c)/(np.sqrt(h*h*b*b*c*c + k*k*a*a*c*c + l*l*a*a*b*b))
        #triclinic
        elif lattice_type == 3:
            alpha = a2r * alpha
            beta  = a2r * beta
            gamma = a2r * gamma
            V=a*b*c*np.sqrt(1- (cos(alpha)**2+ cos(beta)**2 + cos(gamma)**2) + 2*cos(alpha)*cos(beta)*cos(gamma))
            S11=b*b*c*c*np.sin(alpha)*np.sin(alpha)
            S22=(a*c*sin(beta))**2
            S33=(a*b*sin(gamma))**2
            S12=a*b*(c*c)*(cos(alpha)*cos(beta)-cos(gamma))
            S23=c*b*(a*a)*(cos(beta)*cos(gamma)-cos(alpha))
            S13=a*c*(b*b)*(cos(gamma)*cos(alpha)-cos(beta))
            #print h,k,l
            denom=(S11*(h**2)+S22*(k**2)+S33*(l**2)+2*S12*h*k+2*S23*k*l+2*S13*h*l)

            dhkl=V*np.sqrt(1/denom)
        else:
            print 'Lattice type',lattice_type,' not added to get_dhkl. Will exit.'
            exit()
        #print "d_",h,k,l,'=',dhkl
        return dhkl

    #def make_nacl_cell(self):

    def compute_hkllist(self,resfile,dmin):
        cell_vec=self. get_cell_vec(resfile)
        #print cell_vec
        #should be max (norm of l1,l2,l3)
        dmax=cell_vec.max()
        U_frac=self.get_fractional_coords(resfile)
        boxes_around_cell=self.count_cells([0,0,0],dmax,cell_vec,U_frac)

        return boxes_around_cell

        #U_im=self.make_image(U_frac,boxes_around_cell,cell_vec,dmax)


    def compute_atomic_scattering_factor(self, dhkl,element):
        #print '>>in compute_at_scat_fac'

        #see pg 295, Structure of materials:...............
        carbon=['C','C1','C2' ,'C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','C16','C17','C18','C19','C20']
        hydrogen=['H','H1','H2','H3','H4', 'H5','H6', 'H7', 'H8', 'H9', 'H10', 'H11', 'H12', 'H13', 'H14', 'H15', 'H16','H17','H18', 'H19', 'H20']
        oxygen=['O','O1','O2','O3','O4','O5','O6','O7','O8','O9','O10']
        chlorine=['Cl','Cl1']
        sodium=['Na']
        #ai=a1,a2,a3,a4
        #bi=b1,b2,b3,b4
        #all values in angstorm -1 (note: wavelngth in Angstroms)
        if element in carbon:
            a=[0.731,1.195,0.456,0.125]
            b=[36.995,11.297,2.814,0.346]
            Z=6
        elif element in hydrogen:
            a=[0.202,0.244,0.082,0]
            b=[30.868,8.544,1.273,0]
            Z=1
        elif element in oxygen:
            a=[0.455,0.917,0.472,0.138]
            b=[23.780,7.622,2.144,0.296]
            Z=8
        elif element in chlorine:
            a=[1.452,2.292,0.787,0.322]
            b=[30.935,9.980,2.234,0.323]
            Z=17
        elif element in sodium:
            a=[2.241,1.333,0.907,0.286]
            b=[108.004,24.505,3.391,0.435]
            Z=11
        else:
            print 'Atom name not found in compute_atomic_scattering_factor. Exit'
            exit()


        s=1/(2*dhkl)
        #print 'sin(theta)/lamba=',s
        f_s=Z - 41.78214*(s**2)*(a[0]*np.exp(-b[0]*(s**2))+a[1]*np.exp(-b[1]*(s**2))+a[2]*np.exp(-b[2]*(s**2))+a[3]*np.exp(-b[3]*(s**2)))
        #print "Atomic scattering factor for", element,'=',f_s,''
        return f_s

    def get_elements(self,resfile):
        X=CrystalStructure()
        X.init_from_res_file(resfile)
        element = [x.clean_label() for mol in X.unique_molecule_list
                                      for x in mol.atoms]
        #print "natoms=", len(element)
        #print element
        return element

    def compute_structure_factor(self,resfile,h,k,l):
        #see J. Appl. Cryst, (1999),32,864, pg 866
        #returns structure factor for a given Bragg reflection (h,k,l)
        #no crystal symmetry other than translational is assumed. dhkl is calculated for triclinic systems.
        #print '\n >>in compute_structure_factor \n'
        lattice_type=self.get_lattice_type(resfile)
        cell=self.get_cell_parameters(resfile)
        a=cell[0]
        b=cell[1]
        c=cell[2]
        alpha=cell[3]
        beta=cell[4]
        gamma=cell[5]
        nacl_Test=False
        if nacl_Test:
            a=self.nacl_a()
            dhkl= (a)/(np.sqrt(h*h +k*k +l*l))
            print 'dhkl_nacl=',dhkl
            U_frac=self.nacl_fractionals()
            element=self.nacl_elements()
            print element
        else:

            dhkl=self.get_dhkl(lattice_type,h,k,l,a,b,c,alpha,beta,gamma)
            U_frac=self.get_fractional_coords(resfile)
            element = self.get_elements(resfile)




        #get element. Make complete edictionary for this in compute_atomoc_scattering_factor.

        #first part to do with dhkl and temperatre factor
        natoms=len(U_frac)
        #print 'natoms=', natoms

        fi=[]
        #second part is summing over all atoms. with atomic scattering factor calculated on the fly for each atom.

        F_hkl2=0
        sum_cosine=0
        sum_sine=0
        sum=0
        pi=np.pi
        for i in range(0,natoms):
            fi=self.compute_atomic_scattering_factor(dhkl,element[i])
           # print '**', element[i]

            cosine_part=fi*np.cos(2*pi*(h*U_frac[i,0]+k*U_frac[i,1]+l*U_frac[i,2]))
            #print 'cosine_part=', cosine_part
            sine_part=fi*np.sin(2*pi*(h*U_frac[i,0]+k*U_frac[i,1]+l*U_frac[i,2]))
            #print 'sine_part',sine_part
            sum_cosine=sum_cosine+cosine_part
            sum_sine=sum_sine+sine_part

        #print 'sum_cosine=',sum_cosine
        #print 'sum_sine=',sum_sine
        #returns sin**2+cos**2 part of the structure factior
        F_hkl2=sum_cosine*sum_cosine + sum_sine*sum_sine

        #print 'Structure factor squared =',F_hkl2
        #squared of structure factor
        return F_hkl2

    def get_debye_waller(self):
        f=0.5 #angstrom**2
        return f
    def get_absorption_factor(self,linear_absorption_coefficient):
        A=1/(2*linear_absorption_coefficient)

        return A
    def get_multiplicity_factor(self,h,k,l):
        #see pg 312 (basically divide by number of hkl)
        #check type of lattice.
        M=1
        return M

    def get_lorentz_polrisation_factor(self,theta):
        a2r = 0.0174532925
        theta=a2r*theta
        num=(1+(cos(2*theta))**2)
        denom=((sin(theta))**2)*(cos(theta))
        #print theta,denom,num
        if denom==0:
            #print theta
            print "Zero denominator encountered in LP calculation. WTF. Exit."
            exit()
        lorentz_factor = num / denom

#       print lorentz_factor

        #if lorentz_factor <0  :
          #  print 'LP factor is negative. WTF!. Exit'
          #  exit()

        return lorentz_factor


    def get_cell_parameters(self,resfile):
        #returns a,b,c in angstroms and alpha,beta,gamma in radians
        X=CrystalStructure()
        mol=Molecule()
        #print X.cell
        X=CrystalStructure()
        mol=Molecule()
        init=X.init_from_res_file(resfile)
        res_data=X.res_list_form(1.0,False)
        cell= res_data[1].split()
        a2r = 0.0174532925
        a=float(cell[2])
        b=float(cell[3])
        c=float(cell[4])
        alpha=float(cell[5])
        beta=float(cell[6])
        gamma=float(cell[7])
        alpha=a2r*alpha
        beta=a2r*beta
        gamma=a2r*gamma
        cell=[a,b,c,alpha,beta,gamma]
        return cell

    def compute_intensity_hkl(self,h,k,l,resfile,lpfactor,scaling_factor,temp_factor,wavelength):
        #I(khl)= Scaling_factore*Lorentz-polarization factor*|structure factor|**2
        #Three correctoion factors can be apploied to the computed structure factor.
        #1)'Temperature factor (Debye-waller)
        #0.005nm2 is default.
        f = open ('hkl.dat','a')
        temp_factor=self.get_debye_waller()

        #'2)Absorption factor
        absorp_factor=self.get_absorption_factor(linear_absorption_coefficient=1)
        #3) Multiplicity factor
        #get lattice type
        lattice_type=self.get_lattice_type(resfile)
        multiplicity_factor=self.get_equivalent_reflections(lattice_type,h,k,l)
        print h,k,l,multiplicity_factor

        #4) Lorentz-polarization factor:
        #print h,k,l

        theta=self.get_twotheta(h,k,l,resfile,wavelength)/2 #in degrees
        lp_factor=self.get_lorentz_polrisation_factor(theta)
        nacl_Test=False
        cell=self.get_cell_parameters(resfile)
        a=cell[0]
        b=cell[1]
        c=cell[2]
        alpha=cell[3]
        beta=cell[4]
        gamma=cell[5]
        if nacl_Test:
            lp_factor =1

        F_hkl_squared=self.compute_structure_factor(resfile,h,k,l)

        #print 'Multiplicity factor=', multiplicity_factor
        ###testing with nacl:
        #print 'absorp_factor=',absorp_factor
        #print 'lpfactor=',lp_factor
        #print 'scaling factor=',scaling_factor

        constant=scaling_factor*lp_factor*multiplicity_factor*absorp_factor*temp_factor
        #I_hkl=scaling_factor*lpfactor*(np.absolute(F_hkl))**2
        #print 'constant=',constant
        #print 'Structure factor squared =', F_hkl_squared

        I_hkl=F_hkl_squared*constant

        #print "Intensity for",h,k,l,'=',I_hkl
        d_hkl=self.get_dhkl(lattice_type,h,k,l,a,b,c,alpha,beta,gamma)
        f.write('%d %d %d %2.3f %2.3f %d %2.3f %2.3f\n'%(h,k,l,d_hkl,I_hkl,multiplicity_factor,F_hkl_squared,constant ))
        return I_hkl


    def get_twotheta(self,h,k,l,resfile,wavelength):
            r2a=180/np.pi
            #wavelength=1.5418 #Cu Kalpha in angstroms.
            lattice_type=self.get_lattice_type(resfile)
            cell=self.get_cell_parameters(resfile)
            a=cell[0]
            b=cell[1]
            c=cell[2]
            alpha=cell[3]
            beta=cell[4]
            gamma=-cell[5]
            dhkl=self.get_dhkl(lattice_type,h,k,l,a,b,c,alpha,beta,gamma)
            #print wavelength,dhkl
            if dhkl<wavelength/2 :
                print "dhkl cannot be less than wavelength/2!. Exit"
                exit()
            theta=2*math.asin(wavelength/(2*dhkl))
#            print 'Two-theta=',2*theta*r2a, 'degrees'
            return theta*r2a #degrees

    def compute_intensity_2theta(self,h,k,l,resfile,wavelength):
        #we need to sum over all hkl values here???
        twotheta=self.get_twotheta(h,k,l,resfile,wavelength)
        return twotheta

    def get_lattice_type(self,resfile):
        #classify into one of 7 bravais lattice systems from resfile:
        #NOTATION USED IN DIFFRACTION.PY
        #1 CUBIC
        #2 MONOCLINIC
        #3 TRICILIC
        #4 ORTHORHOMBIC
        #5TETRAGONAL
        #6 RHOMBOHEDRAL
        #7 HEXAGONAL


        X=CrystalStructure()
        mol=Molecule()
        #print X.cell
        X=CrystalStructure()
        mol=Molecule()
        init=X.init_from_res_file(resfile)
        res_data=X.res_list_form(1.0,False)
        cell= res_data[1].split()
        a2r = 0.0174532925
        a=float(cell[2])
        b=float(cell[3])
        c=float(cell[4])
        alpha=float(cell[5])
        beta=float(cell[6])
        gamma=float(cell[7])
        length = a, b, c
        angle=alpha, beta, gamma
        if all(i == 90 for i in angle) and a==b==c:
            #print "cubic"
            lattice=1
        elif alpha==gamma==90 and beta!=90:
            #print 'Monoclinic'
            lattice=2
        elif alpha!=gamma!=beta:
            #print 'Triclinic'
            lattice=3
        elif alpha==gamma==beta==90 and a!=b!=c:
            #print 'Orthorhombic'
            lattice=4
        elif alpha==gamma==beta==90 and a!=c:
            #print 'Tetragonal'
            lattice=5
        elif alpha==beta==gamma!=90 and a==b==c:
            #print 'Rhombohedral'
            lattice=6
        elif alpha==beta==90 and a==b!=c and gamma==120:
            #print 'Hexagonal'
            lattice=7
        #print 'Lattice type in random notation=',lattice


        return lattice

    def get_equivalent_reflections(self,lattice_type,h,k,l):
        #print '>>in get_equivalent_reflections'
        #pg 312 Structure of materials........
        #make a dictionary:makes lifg
        lattice=lattice_type
        #cubic lattice:
        #hkl  hhl hh0 0kk hhh hk0 h0l 0kl h00 0k0 00l
        if lattice==1:
            print 'In cubic lattice'
            if h!=k!=l:
                r=48
            elif h==k!=l:
                r=24
            elif h==k and l==0l:
                r=12
            elif h==0 and k==l:
                r=12
            elif h==k==l:
                r=8
            elif h!=k and l==0:
                r = 24
            elif h!=l and k==0:
                r=24
            elif h==0 and k!=l:
                r=24
            elif h!=k and h!=l and k==l==0:
                r=6
            elif h==l==0 and k!=h and k!=l:
                r=6
            elif h==k==0 and l!=h and l!=k:
                r=6

        #tetragonal lattice:
        elif lattice==5:
            print 'in tetragonal lattice'
            if h!=k!=l:
                r=16
            elif h==k!=l:
                r=8
            elif h==k and l==0l:
                r=4
            elif h==0 and k==l:
                r=8
            elif h==k==l:
                r=8
            elif h!=k and l==0:
                r = 8
            elif h!=l and k==0:
                r=8
                #0kl
            elif h==0 and k!=l:
                r=8
                #h00
            elif h!=k and h!=l and k==l==0:
                r=4
            elif h==l==0 and k!=h and k!=l:
                r=4
            elif h==k==0 and l!=h and l!=k:
                r=2
            else:
                r=1
        #orthorhombic
        elif lattice==4:
            print 'in orthorhombic lattice'

            if h!=k and k!=l :
                #hkl
                r=8
            elif h==k and k!=l and h!=0 and k!=0:
                #hhl
                r=8
            elif h==k and l==0 and h!=0:
                #hh0
                r=8
            elif h==0 and k==l and l!=0:
                #0kk
                r=8
            elif h==k and k==l:
                #hhh
                r=8
            elif h!=k and l==0 and h!=0 and k!=0:
                #hk0

                r = 4
            elif h!=l and k==0 and h!=0 and l!=0:
                r=4

                #0kl
            elif h==0 and k!=l and l!=0 and k!=0 and l!=0:
                #0kl
                r=4
            elif h!=k and h!=l and k==l and l==0:
                #h00
                r=2
            elif h==l and l==0 and k!=0 :
                #0k0
                r=2
            elif h==0 and k==0 and l!=0:
                #00l
                r=2
            else:
                print 'Warning:Cannot assign multiplicity factor. Setting it to 1'
                r=1
        else:
            print 'lattice type',lattice,'not added to get_equivalent_reflections. EXIT'
            exit()


        return r



    def get_all_intensity_hkl(self,hkllist,resfile,wavelength):
        #for all hkl in hkllist, finds intensity and twotheat value and stores in file

        f = open ('xrd.dat','w')
        print hkllist
        hmin,kmin,lmin=0,0,0
        hmax=hkllist[0]
        kmax=hkllist[1]
        lmax=hkllist[2]
        hlist=np.arange(0,hmax+1)
        klist=np.arange(0,kmax+1)
        llist=np.arange(0,lmax+1)
        Ihkl=[]
        twotheta=[]
        hkl=[]
        f.write('%s\t %s\t %s\t  %s\t %s\n'%('intensity','2theta','h','k','l' ))

        for h in hlist:
            for k in klist:
                for l in llist:
                    print h,k,l
                    if h==0 and k==0 and l==0:
                        continue
                    else:
                        intensity=self.compute_intensity_hkl(h,k,l,resfile,lpfactor=1,scaling_factor=1,temp_factor=1,wavelength=1.54184)
                        Ihkl.append(intensity)
                        angle=self.get_twotheta(h,k,l,resfile,wavelength=1.54184)
                        twotheta.append(angle)
                        #if angle <
                        hkl.append([h,k,l])
                        if intensity > 0 :
                            f.write('%2.5f\t %2.5f\t %d \t %d\t %d\n'%(intensity,angle,h,k,l ))


        #print 'Max. intensity =',Imax

        scale=True
        if scale:
            print 'scaling is true'
            Imax=np.amax(Ihkl)
            scaled_intensity=np.array(Ihkl)/Imax






        all_together=[Ihkl,twotheta,hkl]
        f.close()
#        print all_together





        return [Ihkl,twotheta,]
#        klist=np.arange[kmax]
#        llist=np.arange[lmax]
    #def write_to_file(self,):



#routines for testing with nacl
    def nacl_fractionals(self):
        a=[0, 0, 0, 0, 0.5, 0.5, 0.5, 0, 0.5, 0.5, 0.5, 0, 0.5, 0, 0, 0, 0.5, 0, 0, 0, 0.5, 0.5, 0.5, 0.5]
        U_frac=np.reshape(a, (8, 3))
        return U_frac

    def nacl_elements(self):
        #testing with nacl.
        #element=[]
        element=['Na','Na' ,'Na','Na','Cl','Cl','Cl','Cl']
        return element
    def nacl_a(self):
        return 5.6402
    def nacl_b(self):
        return 5.6402
    def nacl_c(self):
        return 5.6402
    def nacl_alp(self):
        return 90
    def nacl_bet(self):
        return 90
    def nacl_gam(self):
        return 90
def main():

    #initialise class XRDCalculator
    xrd=XRDCalculator()
    parser = argparse.ArgumentParser()
    parser.add_argument("-res_xrd", "--resfile_for_xrd", help="resfile name for XRD calculation" )
    parser.add_argument("-res_xrd_sl", "--resfile_for_xrd_sl", help="Gen XRD pattern for all resfiles in structure list" )
    args = parser.parse_args()
    if args.resfile_for_xrd:
        if os.path.isfile(args.resfile_for_xrd):
            wavelength=1.54184  #CuKa - add arparse inut for wavelength of incident beam.,
            print "resfile for xrd calculation :", args.resfile_for_xrd
            resfile=args.resfile_for_xrd
            dmin=0.5
            hkllist=xrd.compute_hkllist(resfile,dmin)
            xrd.get_all_intensity_hkl(hkllist,resfile,wavelength)
        else:
            print 'File', args.resfile_for_xrd,' not found. Exit.'
            exit()
    if args.resfile_for_xrd_sl:
        print 'structure list format selected'
        if os.path.isfile(args.resfile_for_xrd_sl):
            wavelength=1.54184  #CuKa - add arparse inut for wavelength of incident beam.,
            print "resfile for xrd calculation :", args.resfile_for_xrd
            resfile=args.resfile_for_xrd
            dmin=0.5
            hkllist=xrd.compute_hkllist(resfile,dmin)
            xrd.get_all_intensity_hkl(hkllist,resfile,wavelength)


    #generate reciprocal_cell_vectors
#    l=xrd.get_xrd_data(structure,scaled=True,two_theta_range=(0,90))


    #l=xrd.get_cell_vec(resfile)

    #recip_l=xrd.recip_cell(l)
    #U_frac=xrd.get_fractional_coordsget_fractional_coords('test.res')
    #U=[0,0,0]
    #R0=10
    #boxes=xrd.count_cells(U,R0,l,U_frac)
    #recip_pts=xrd.make_image(U,boxes,l,R0)
    #print "Recip vectors:", recip_l
    #print "No. of Reciprocal points--",len(recip_pts)
    #g_110=xrd.get_gkhl(recip_l,1,1,0)

    #wavelength=0.154184 #CuKa
    two_theta_range=(0,30)#two-theta range
    # dhkl=xrd.get_dhkl(resfile,1,1,1)
    #print "s=sin(theta)/wavelength = ",1/(2*dhkl)
    #sinf(theta)/la   mbda)=1/2d
    element = 'C'
    dmin=0.5

    #here all intenity.
    #xrd.get_all_intensity_hkl(hkllist,resfile,wavelength)
#    print hkllist
    h=1
    k=1
    l=1

    #Ihkl=xrd.compute_intensity_hkl(h,k,l,resfile,lpfactor=1,scaling_factor=1,temp_factor=1,wavelength=1.54184)
    twothetahkl=xrd.get_twotheta(h,k,l,resfile,wavelength)


    #eliminate equivalent reflections
    #xrd.get_equivalent_reflections(resfile,h,k,l)
    #xrd.get_all_intensity_hkl(hkllist,resfile,wavelength)
    print 'See xrd.dat and hkl.dat'





   #y=kp.find_spgr(structures)

main()